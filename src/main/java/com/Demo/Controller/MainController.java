package com.Demo.Controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@RequestMapping("/sayhello")

	
	public static String sayhello() throws UnknownHostException {
		
		InetAddress inetAddress = InetAddress. getLocalHost();
		
		return "hello" +"   "+inetAddress. getHostName() +"   "+inetAddress. getHostAddress();
	}
}
