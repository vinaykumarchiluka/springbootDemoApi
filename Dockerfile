FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD target/springbootDemoApi.jar springbootDemoApi.jar
ENTRYPOINT ["java", "-jar","target/springbootDemoApi.jar"]